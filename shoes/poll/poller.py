import django
import os
import sys
import time
import json
import requests

sys.path.append("") # This code allows us to treat the code UNDER IT as if it is already in the Hats project
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
from shoes_rest.models import BinVO



def get_bins():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    print(content)
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"], #unique identifier, create vs update
            defaults={"bin_number": bin["bin_number"], # everything else
                       "closet_name": bin["closet_name"]}
        )

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
