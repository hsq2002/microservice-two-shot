from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import BinVO, Shoe
from common.json import ModelEncoder

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href",
                  "bin_number",
                  "closet_name"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = {
        "name",
        "color",
        "picture_url",
        "bin",
    }
    encoders={"bin": BinVODetailEncoder()}
    #  def get_extra_data(self, o): #THIS IS BREAKING THINGS,
    # even when o.bin.name is changed to o.bin_vo.name. Maybe needs o.bin_vo.number
    #      return {"bin": o.bin.name}

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "color",
        "picture_url",
        "bin",
        ]
    encoders={"bin": BinVODetailEncoder()}



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        #print(content) #HOW TO PRINT THIS CONTENT??
                       #DOCKER? "Module 'Django' not imported" when I cd into this directly and try to run this python file
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            #print(bin)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"}, #Currently printing this message -
                                                # content is not getting translated to proper
                                                # BinVO Object (vs href) is my suspiction
                status=400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET",])
def api_show_shoe(request, pk):
    shoes = Shoe.objects.get(id=pk)

    if request.method == "GET":
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    else:
        try:
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
