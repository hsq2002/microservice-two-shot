from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    bin_number = models.PositiveIntegerField(null=True, blank=True)
    closet_name = models.CharField(max_length=100, null=True, blank=True)



    def __str__(self):
        return f"BinVO {self.bin_number}"


class Shoe(models.Model):

    name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )

    def __str__(self):
        return f"{self.name}"

    class Meta:
         ordering = ["name"]
