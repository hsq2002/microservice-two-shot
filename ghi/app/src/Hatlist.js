import React, { useState, useEffect } from "react";


const HatList = () => {
    const[hatList, setHatList] = useState([]);
    const fetchData = async () => {
      const url = "http://localhost:8090/api/hats_rest/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setHatList(data)
        }
      };

      useEffect(() => {
        fetchData();
      }, []);

      const handleDelete = async (id) => {
        const url = "http://localhost:8090/api/hats_rest/${id}/";
        const fetchConfig = {
          method: "delete",
          headers: {
            "Content-Type": "application/json",
          },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          fetchData();
        } else {
          alert("Hat was not detected!");
        }
      };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Location</th>
                    <th>id</th>
                    <th>color</th>
                    <th>fabric</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat =>{
                  return(
                    <tr key={hat.href }>
                        <td>{ hat.name }</td>
                        <td>{ hat.location }</td>
                        <td>{ hat.id }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.fabric }</td>
                    </tr>
                  );
                })}
            </tbody>
        </table>
    )
            }
export default HatList;
