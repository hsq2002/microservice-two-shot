from django.db import models
from django.core.exceptions import ObjectDoesNotExist
# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class Hat(models.Model):
    name = models.CharField(max_length=200)
    id = models.PositiveIntegerField(primary_key=True)
    color = models.CharField(max_length=200, null=True, blank=True)
    picture_url = models.URLField(null=True)
    fabric = models.CharField(max_length=200, null=True, blank=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="+",
        on_delete=models.PROTECT,
        null=True
    )

    def __str__(self):
        return f"{self.name}"
