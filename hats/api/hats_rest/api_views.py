from .models import LocationVO, Hat
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from django.http import JsonResponse


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "name"
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "picture_url",
        "id",
        "color",
        "fabric",
    ]

    # def get_extra_date(self, o):
    #     return {"Hat": o.Hat.name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "picture_url",
        "id",
        "color",
        "fabric",
    ]

    encoders = {
        "Location": LocationVOEncoder()

    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        try:

            content = json.loads(request.body)
            hats = Hat.objects.create(**content)
            return JsonResponse(
                hats,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse (str(e), safe=False)

@require_http_methods(["DELETE", "GET"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        try:
            count, _ = Hat.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
